/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import clases.LeerArchivo;
import RedPetri.Redes;
import clases.Ficheros;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 *
 * @author Pato
 */
public class SystemTest {
    
    public SystemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void sensores() {
        int[][] iEsperado = {{0,0,0,0,0,1,0,0,0,1,0,1,0,0}};
        LeerArchivo leerArchivo = new LeerArchivo("PrioridadSensores.xls","AutomaticasSensores.xls","Sensores.html","TiemposSensores.xls");
        HashMap<String,int[][]> datos =  leerArchivo.LeerHTML();
        Redes oRed = new Redes(datos.get("marcado"), datos.get("incidencia"),datos.get("inhibicion"),datos.get("tiempos"),datos.get("automaticas"));
        oRed.ejecutar(new int[][]{{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}});
        try {
            Thread.sleep(160);
        } catch (InterruptedException ex) {
            Logger.getLogger(SystemTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        oRed.ejecutar(new int[][]{{0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0}});
        oRed.ejecutar(new int[][]{{0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0}});
        int[][] iReal  = oRed.getNuevoMarcado();
        for (int i = 0; i < iEsperado[0].length; i++) {
            assertEquals(iEsperado[0][i],iReal[0][i]);        
        }
        try 
        {
            File path = new File (System.getProperty("user.dir") + "\\" + Ficheros.Instance().getsArchivo());
            Desktop.getDesktop().open(path);
        }catch (IOException ex)
        {        }
    }
    
    @Test
    public void bomba() {
        int[][] iEsperado = {{0,0,0,0,1,0,0,0,0,1,0,0}};
        LeerArchivo leerArchivo = new LeerArchivo("PrioridadBomba.xls","AutomaticasBomba.xls","Bomba.html","TiemposBomba.xls");
        HashMap<String,int[][]> datos =  leerArchivo.LeerHTML();
        Redes oRed = new Redes(datos.get("marcado"), datos.get("incidencia"),datos.get("inhibicion"),datos.get("tiempos"),datos.get("automaticas"));
        oRed.ejecutar(new int[][]{{0,0,0,0,0,0,0,0,0,0,1,0,0}});
        try {
            Thread.sleep(6000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SystemTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        oRed.ejecutar(new int[][]{{0,0,0,0,0,0,0,0,0,0,1,0,0}});
        oRed.ejecutar(new int[][]{{0,0,0,0,0,0,0,0,1,0,0,0,0}});
        oRed.ejecutar(new int[][]{{0,0,1,0,0,0,0,0,0,0,0,0,0}});
        int[][] iReal  = oRed.getNuevoMarcado();
        for (int i = 0; i < iEsperado[0].length; i++) {
            assertEquals(iEsperado[0][i],iReal[0][i]);        
        }
        try 
        {
            File path = new File (System.getProperty("user.dir") + "\\" + Ficheros.Instance().getsArchivo());
            Desktop.getDesktop().open(path);
        }catch (IOException ex)
        {
        }
    }
}
