/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import RedPetri.Politicas;
import RedPetri.Redes;
import clases.Matriz;
import clases.Ficheros;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * 
 */
public class UnitTests {
    
    int[][] iMarcadoInicial = {{0, 0, 1, 1}};
    
    int[][] iH = {{0, 0, 1, 0, 0},
                  {1, 0, 0, 0, 0},
                  {0, 0, 0, 1, 0},
                  {0, 1, 0, 0, 0}};
        
    int[][] iIncidencia =  {{-1, 1, 0, 0, 0},
                            {0, 0, 1, -1, 0},
                            {1, -1, 0, 0, 0},
                            {1, 0, -1, 1, -1}};
    
    int[][] iTiempos = {{0,0},{0,0},{150,200},{0,0},{0,0}};
    
    public UnitTests() {
    }
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void testDispararNOsensibilizada() 
    {    
        Ficheros.Instance().Escribir("TEST","DISPARAR_NO_SENSIBILIZADA");
        int[][] iDisparo  = {{0, 0, 0, 1, 0}};
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH,null);
        Matriz oEsperada = new Matriz(iMarcadoInicial);        
                
        oRed.ejecutar(iDisparo,true);
        Matriz oReal =  new Matriz(oRed.getNuevoMarcado());
        
        for (int i = 0; i < oReal.getFilCount(); i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperada.getVal(i, j), oReal.getVal(i, j));                
            }            
        }        
    }
    
    @Test
    public void testDispararSensibilizada() 
    {
        Ficheros.Instance().Escribir("TEST","DISPARAR_SENSIBILIZADA");
        int[][] iDisparo  = {{0, 0, 1, 0, 0}};
        int[][] iEsperada = {{0, 1, 1, 0}};
        
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH,null);
        oRed.ejecutar(iDisparo,true);        
        
        Matriz oEsperada = new Matriz(iEsperada);
        Matriz oReal =  new Matriz(oRed.getNuevoMarcado());
        
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperada.getVal(i, j), oReal.getVal(i, j));                
            }            
        }        
    }
    
    @Test
    public void testTransSensibilizadas() 
    {
        Ficheros.Instance().Escribir("TEST","OBTENER_SENSIBILIZADAS");
        int[][] iEsperada = {{0, 0, 1, 0,1}};
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH,null);
                
        Matriz oReal = oRed.getSensibilizadas();
        Matriz oEsperada = new Matriz(iEsperada);
        
        for (int i = 0; i < oReal.getFilCount(); i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperada.getVal(i, j), oReal.getVal(i, j));                
            }            
        }
    }  
    
    @Test
    public void testPrioridad() 
    {
        Ficheros.Instance().Escribir("TEST","PRIORIDAD");
        int[][] iDisparo  = {{0, 0, 1, 0, 1}};
        int[][] iPolitica = {{0,0,1,0,0}, //3-4-1-5-2 Esta es la prioridad
                             {0,0,0,0,1},
                             {1,0,0,0,0},
                             {0,1,0,0,0},
                             {0,0,0,1,0}};
        int[][] iEsperado = {{0, 0, 1, 0, 0}};
        Politicas oPolitica = Politicas.Instance(iPolitica);
        
        Matriz oEsperada = new Matriz(iEsperado);
        Matriz oReal =  new Matriz(oPolitica.TransicionAEjecutar(new Matriz(iDisparo)));
        //Matriz oAEjecutar = new Matriz(oPolitica.TransicionAEjecutar(oSensibilizadas, iPolitica));
        
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperada.getVal(i, j), oReal.getVal(i, j));
                
            }
            
        }
    }
    

    
    @Test
    public void testNoSensibilizadoPorTiempo()
    {
        Ficheros.Instance().Escribir("TEST","NO SENSIBILIZADA POR TIEMPO");
        
        int[][] iTiempos = {{150,200},{0,0},{0,0},{0,0},{0,0}};
        int[][] iEsperado = {{0,1,1,1,1}};
        Matriz oEsperado = new Matriz(iEsperado);
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH, iTiempos, null);
        
        int[][] iReal = oRed.getTemporales();
        Matriz oReal = new Matriz(iReal);
        
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperado.getVal(i, j), oReal.getVal(i, j));                
            }            
        }
        
    }
    
    @Test
    public void testSensibilizadoPorTiempo()
    {
        Ficheros.Instance().Escribir("TEST","SENSIBILIZADA POR TIEMPO");
        
        int[][] iEsperado = {{1,1,1,1,1}};
        Matriz oEsperado = new Matriz(iEsperado);
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH, iTiempos,null);

        try {
            Thread.sleep(160);
        } catch (InterruptedException ex) {
            Logger.getLogger(UnitTests.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int[][] iReal = oRed.getTemporales();
        Matriz oReal = new Matriz(iReal);
        
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperado.getVal(i, j), oReal.getVal(i, j));                
            }            
        }
        
    }
    
    @Test
    public void testAutomatico() 
    {
        Ficheros.Instance().Escribir("TEST","DISPARO_AUTOMATICO");
        int[][] iAutomaticas ={{0, 0, 1, 0, 1}};
        int[][] iEsperada = {{0, 1, 1, 0}};
        
        Redes oRed = new Redes(iMarcadoInicial, iIncidencia, iH,iAutomaticas);   
        
        Matriz oEsperada = new Matriz(iEsperada);
        Matriz oReal =  new Matriz(oRed.getNuevoMarcado());
        
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < oReal.getColCount(); j++) {
                assertEquals(oEsperada.getVal(i, j), oReal.getVal(i, j));                
            }            
        }        
    }
    @After
    public void tearDown() {
    }
}
